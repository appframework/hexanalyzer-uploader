package de.egore911.hexanalyzer.uploader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Scanner;

import javax.swing.JFileChooser;

import org.json.JSONArray;
import org.json.JSONObject;

public class Upload {

	public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
		String[] files;
		if (args.length == 0) {
			JFileChooser chooser = new JFileChooser();
			chooser.setMultiSelectionEnabled(true);
	        chooser.showOpenDialog(null);
	        files = Arrays
					.stream(chooser.getSelectedFiles())
					.map(File::getAbsolutePath)
					.toArray(String[]::new);
		} else {
			files = args;
		}

		URL url = URI.create("http://localhost:8800/rest/binary").toURL();
		for (String file : files) {
			Path path = Paths.get(file);
			byte[] bytes = Files.readAllBytes(path);

            JSONObject binary = new JSONObject();
            binary.put("data", toHex(bytes));
            binary.put("name", path.getFileName().toString());
            JSONArray groupIds = new JSONArray();
            groupIds.put(1);
            binary.put("groupIds", groupIds);
            post(url, binary);
        }
	}

	private static void post(URL url, JSONObject binary)
            throws IOException, NoSuchAlgorithmException {
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
		String encoded = Base64.getEncoder().encodeToString(("admin:" + sha1("admin")).getBytes(StandardCharsets.UTF_8));
		conn.setRequestProperty("Authorization", "Basic " + encoded);
		try (OutputStream os = conn.getOutputStream()) {
			os.write(binary.toString().getBytes(StandardCharsets.UTF_8));
		}
		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			StringBuilder buffer = new StringBuilder();
			buffer.append(conn.getResponseCode());
			buffer.append(" (");
			buffer.append(conn.getResponseMessage());
			buffer.append(")");
			if (conn.getErrorStream() != null) {
				buffer.append(": ");
				buffer.append(new String(conn.getErrorStream().readAllBytes(), StandardCharsets.UTF_8));
			}
			throw new IOException(buffer.toString());
		} else {
			try (InputStream in = conn.getInputStream(); Scanner scanner = new Scanner(in, StandardCharsets.UTF_8)) {
				String inputStreamString = scanner.useDelimiter("\\A").next();
				new JSONObject(inputStreamString);
			}
		}
	}

	private static String sha1(String input) throws NoSuchAlgorithmException {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
			messageDigest.update(input.getBytes());
			byte[] digestBytes = messageDigest.digest();

			// Convert the byte array to a hexadecimal string
			StringBuilder hexString = new StringBuilder();
			for (byte b : digestBytes) {
				String hex = Integer.toHexString(0xff & b);
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}

			return hexString.toString();
	}

	private static StringBuilder toHex(byte[] bytes) {
		String format = "%02X";
		StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
			sb.append(String.format(format, b));
        }
		return sb;
	}

}

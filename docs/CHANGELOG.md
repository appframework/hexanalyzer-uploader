## [1.0.9](https://gitlab.com/appframework/hexanalyzer-uploader/compare/1.0.8...1.0.9) (2025-02-26)

## [1.0.8](https://gitlab.com/appframework/hexanalyzer-uploader/compare/1.0.7...1.0.8) (2025-01-15)


### Bug Fixes

* **deps:** update dependency org.json:json to v20250107 ([30a12cd](https://gitlab.com/appframework/hexanalyzer-uploader/commit/30a12cddec42bee3dac36bb103536b3f367a76fa))

## [1.0.7](https://gitlab.com/appframework/hexanalyzer-uploader/compare/1.0.6...1.0.7) (2025-01-09)


### Bug Fixes

* **deps:** update dependency org.json:json to v20241224 ([9e8b8bd](https://gitlab.com/appframework/hexanalyzer-uploader/commit/9e8b8bd71e99bb307a8f31d0447ae2f8ce4bf32c))

## [1.0.6](https://gitlab.com/appframework/hexanalyzer-uploader/compare/v1.0.5...1.0.6) (2024-11-15)

## [1.0.5](https://gitlab.com/appframework/hexanalyzer-uploader/compare/v1.0.4...v1.0.5) (2024-09-12)

## [1.0.4](https://gitlab.com/appframework/hexanalyzer-uploader/compare/v1.0.3...v1.0.4) (2024-09-12)


### Bug Fixes

* **ci:** Disable distribution ([592791e](https://gitlab.com/appframework/hexanalyzer-uploader/commit/592791e160fee1ce00661a790e23ef2a50145713))
